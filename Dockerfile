FROM golang:alpine

WORKDIR /app

RUN apk update && \
    apk add ca-certificates && \
    rm -rf /var/cache/apk/* && \
    mkdir /usr/local/share/ca-certificates/extra && \
    update-ca-certificates

COPY go.mod go.sum /app/

RUN go mod download

COPY ./ /app/

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o planets-updater main.go

ENV PLANETS_SVC=planets-svc

CMD [ "./planets-updater" ]