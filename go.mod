module gitlab.com/barbosaigor/planets-updater

go 1.14

require (
	github.com/barbosaigor/rq v1.1.0
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/barbosaigor/planets v0.1.0
)
