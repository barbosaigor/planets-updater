package main

import (
	"fmt"
	"os"

	"github.com/barbosaigor/rq"
	"github.com/sirupsen/logrus"
	"gitlab.com/barbosaigor/planets/planet"
	"gitlab.com/barbosaigor/planets/swapi"
)

type appearsJSON struct {
	Appears int `json:"appears"`
}

var planetsSVC = "0.0.0.0"

func init() {
	if svc := os.Getenv("PLANETS_SVC"); svc != "" {
		planetsSVC = svc
	}
}

func main() {
	// Fetch all planets
	logrus.Info("Getting Planets...")
	planets, err := getPlanets(planetsSVC)
	if err != nil {
		logrus.Errorln("Fail to retrieve planets entries of planets service.")
		panic(err)
	}
	// Update all planets
	logrus.Info("Updating Planets...")
	errs := updatePlanets(planetsSVC, planets)
	if len(errs) == 0 {
		logrus.Infof("Success: %d planets updated", len(planets))
		return
	}
	logrus.Errorf("Planets failed:\n%v", errs)
}

func getPlanets(endpoint string) ([]planet.Planet, error) {
	var planets []planet.Planet
	if err := rq.Endpoint(endpoint + "/planets").Get().ToJSON(&planets).Err; err != nil {
		return nil, err
	}
	return planets, nil
}

func updatePlanets(endpoint string, planets []planet.Planet) []error {
	var reqErrs []error
	for _, pln := range planets {
		// Get planet apparitions from SW API
		appears, err := swapi.GetPlanetApparitions(pln.Name)
		if err != nil {
			reqErrs = append(reqErrs, fmt.Errorf("\nPlanet %v %w", pln, err))
			continue
		}
		// Asks to planets service to update the new appears value
		reqData := appearsJSON{appears}
		url := endpoint + "/planets/" + pln.ID
		if err := rq.Endpoint(url).JSON(reqData).Put().Err; err != nil {
			reqErrs = append(reqErrs, fmt.Errorf("\nPlanet %v failed to update: %w", pln, err))
		}
	}
	return reqErrs
}
